package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;

import java.util.Optional;

//this interface will be used to register a user  via userController
public interface UserService {
    //method only no actual logic

    //registeration of a user
    void createUser(User user);

    //check if the user is already exists
    Optional<User> findByUsername(String username);
}
