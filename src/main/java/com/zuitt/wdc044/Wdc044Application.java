package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//managing end points
@RestController
public class Wdc044Application {

	public static void main(String[] args) {

		//this method starts this whole spring framework
		SpringApplication.run(Wdc044Application.class, args);
	}

	//get method
	//http://localhost:8080/hello?name=adobo
	//@RequestParam, get the queury parameters
		//name = john;
		//output - hello john
		//"?" means start of parameters followed by "key-value" pairs
	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s", name);
	};

	@GetMapping("/greet")
	public String greet(@RequestParam(value = "greet", defaultValue = "nice") String name) {
		return String.format("Good %s", name);
	};

	//http://localhost:8080/contactInfo?name=ronel&email=ronel@email.com
	@GetMapping("/contactInfo")
	public String contactInfo(
			@RequestParam(value="name", defaultValue = "user") String name,
			@RequestParam(value="email", defaultValue = "user@email") String email) {
		return String.format("Hello %s, your email is %s", name, email);
	}


	@GetMapping("/hi")
	public String hi(@RequestParam(value="user", defaultValue = "user")String user) {
		return String.format("Hello %s!", user);
	};

	@GetMapping("/nameAge")
	public String nameAge(
			@RequestParam(value="name", defaultValue = "your name") String name,
			@RequestParam(value = "age", defaultValue = "0") int age){
		return String.format("Hello %s! Your age is %d", name, age);
	};

}
