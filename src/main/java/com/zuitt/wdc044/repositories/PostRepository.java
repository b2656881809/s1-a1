package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
//@Repository - it contains the method for data manipulation
//
public interface PostRepository extends CrudRepository<Post, Object> {




}
