package com.zuitt.wdc044.models;

import javax.persistence.*;

//mark this java object as a representation of a database table via
//@entity
@Entity
//designate table name via @Table
@Table(name="posts")
public class Post {

    //indicate that this property represents the primary key via @Id
    @Id
    //values for this property 'id', will be auto incremented
    @GeneratedValue
    private long id;

    //table column in your relational ddatabase
    @Column
    private String title;

    @Column
    private String content;

    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return this.content;
    }

    public void setContent(String content){
        this.content = content;
    }


}
