package com.zuitt.wdc044.models;

import javax.persistence.*;



@Entity
@Table(name="users")
public class User {

    //first column
    @Id
    @GeneratedValue
    private long id;

    //second column
    @Column
    private String username;

    //third column
    @Column
    private String password;

    //default constructor
    public User(){}

    //parameterized constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Long getId(){
        return this.id;
    }

    //setter
    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getPassword(){
       return this.password;
    }




}
